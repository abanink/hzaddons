<?php

define ( 'LOGGER_NORMAL', 0 );
define ( 'LOGGER_TRACE', 1 );
define ( 'LOGGER_DEBUG', 2 );
define ( 'LOGGER_DATA', 3 );
define ( 'LOGGER_ALL', 4 );

function log_level_str($level)
{
	switch($level) {
	case 0: return 'LOGGER_NORMAL';
	case 1: return 'LOGGER_TRACE';
	case 2: return 'LOGGER_DEBUG';
	case 3: return 'LOGGER_DATA';
	case 4:
	default: return 'LOGGER_ALL';
	}
}

?>
