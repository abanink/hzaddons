<?php

interface DiceGame {
	public function roll();
	public function getOutput();
	public function getResult();
}

class DefaultDiceGame implements DiceGame {

	public function __construct($dice, $faces) {
		$this->dice = $dice;
		$this->faces = $faces;
	}

	public function roll() {
		logger('Rolling for default dice game: ' . $this->dice . ' dice, ' . $this->faces . ' faces', LOGGER_DEBUG);

		$cnt = 0;
		$this->result = 0;
		$this->out = '';

                while ( $cnt < $this->dice ) {
                        $r = rand(1,$this->faces);
                        $this->result += $r;
                        $this->out .= ($cnt == 0 ? '' : '+').$r;
                        $cnt++;
                }
	}

	public function getOutput() {
		return $this->out;
	}

	public function getResult() {
		// in case of 1 dice, the result is redundant with the output
		if ( $this->dice == 1 ) return '';

		return $this->result;
	}

	private $dice;
	private $faces;
	private $out;
	private $result;
}

class FudgeDiceGame implements DiceGame {

	public function __construct($dice) {
		$this->dice = $dice;
	}

	public function roll() {
		logger('Rolling for fudge dice game: ' .$this->dice . ' dice', LOGGER_DEBUG);

		$cnt = 0;
		$face_map_values = array(
			1 => -1,
			2 => 0,
			3 => 1
		);
		$face_map_output = array(
			-1 => '-',
			0 => '_',
			1 => '+'
		);

		$this->result = 0;
		$this->out = '';

		while ( $cnt < $this->dice ) {
			$r = rand(1,3);
		        $value = $face_map_values[$r];	
                        $this->result += $value;
                        $this->out .= ($cnt == 0 ? '' : ',').$face_map_output[$value];
                        $cnt++;
                }
	}

	public function getOutput() {
		return $this->out;
	}

	public function getResult() {
		return strval($this->result);
	}

	private $dice;
	private $out;
	private $result;
}

?>
